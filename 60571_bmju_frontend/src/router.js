import { createRouter, createWebHistory } from "vue-router";
import Homepage from "./components/Homepage";
import MainComponent from "./components/MainComponent";
import LoginDialog from "@/components/LoginDialog";

const routes = [
    {
        path: "/",
        component: Homepage,
    },
    {
        path: "/mainComponent",
        component: MainComponent,
    },
    {
        path: '/login',
        component: LoginDialog,
    }

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;