import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios';
import router from './router.js'
import store from './state.js';
import PrimeVue from "primevue/config";
import "primevue/resources/themes/bootstrap4-light-blue/theme.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "primevue/resources/primevue.min.css";
import ConfirmationService from 'primevue/confirmationservice';
import Tooltip from 'primevue/tooltip';

const myApp = createApp(App);
window.axios = axios;
myApp.use(store)
myApp.use(PrimeVue);
myApp.use(ConfirmationService);
myApp.directive('tooltip', Tooltip);
myApp.use(router)
myApp.mount('#app');