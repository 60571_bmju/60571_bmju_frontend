import {createStore} from "vuex";
import axios from "axios";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;



const  store = createStore({
    state: {
        user: {},
        token: null,
        preLoading: false,
        dataPreLoading: true,
        loginError: false,
        loggedIn: false,
        books: {},
        pager: {
            currentPage: 1,
            perPage: 5,
        },
        validation: {},
        createBookDialogVisible: false,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },

        getToken(context, token) {
            context.token = token;
        },

        setUser(context, user) {
            context.user = user;
        },

        setPreloading(context, is_load) {
            context.preLoading = is_load;
        },
        setDataPreloading(context, is_load) {
            context.dataPreLoading = is_load;
        },
        setLoginError(context, isError) {
            context.loginError = isError
        },
        setLoggedIn(context, isLoggedIn) {
            context.loggedIn = isLoggedIn
        },
        setBooks(context, books) {
            context.books = books
        },
        setPage(context, page) {
            context.pager.currentPage = page
        },
        setPager(context, pager) {
            context.pager = pager
        },
        setValidation(context, validation) {
            context.validation = validation;
        },
        setCreateBookDialogVisible(context, visible) {
            context.createBookDialogVisible = visible;
        },
        logout(context) {
            context.user = null;
            context.token = null;
            context.loggedIn = false;
            context.books = {};
            localStorage.removeItem('token');
            router.push('/')
        },
    },
    actions: {
        auth(context, {login, password}){
            // context.commit('setErrorPassword', false)
            context.commit("setPreloading", true);
            axios.post(backendUrl+'/OAuthController/Authorize', {
                username: login,
                password: password,
                grant_type: 'password'
            }, {
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    Authorization:
                        "Basic " + window.btoa(client_id + ":" + client_secret),
                },
            }).then((response) => {
                if(response.data.access_token){
                    context.commit('setToken', response.data.access_token)
                    localStorage.setItem('token', response.data.access_token);
                    context.commit('setLoggedIn', true);
                    context.commit('setLoginError', false);
                    context.dispatch('getUser');
                    router.push('/');

                } else {
                    context.commit('setLoginError', true)
                    context.commit('setPreloading', false)
                }
            })
        },
        getUser(context){
            context.commit('setPreloading', true);
            console.log('get user')
            return window.axios.get(backendUrl+'/OAuthController/user', {
                headers: {
                "Access-Control-Allow-Origin": "*",
                Authorization: 'Bearer ' + context.state.token
                }
            }).then((response) => {
                context.commit('setUser', response.data);
                localStorage.setItem('user', response.data);
                context.commit('setPreloading', false);

            }).catch(error => {
                    if (error.response) context.commit('setLoggedIn', false);
                }
            )
        },
        getBooks(context) {
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('per_page', context.state.pager.perPage)
            return window.axios.post(backendUrl + '/BooksApiController/book?page_group1=' + context.state.pager.currentPage, params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                context.commit('setBooks', response.data.books);
                context.commit('setPager', response.data.pager);
                context.commit('setDataPreloading', false)
                console.log(response.data.books)
                console.log(response.data.pager)

            })
        },
        createBook(context, {Наименование, Автор}) {
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('Наименование', Наименование)
            params.append('Автор', Автор)
            return window.axios.post(backendUrl + '/BooksApiController/store', params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 201) {
                    console.log("Book created successfully");
                    context.commit("setValidation", {})
                    context.commit('setCreateBookDialogVisible', false);
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getBooks');
                    router.push('/mainComponent');
                } else {
                    console.log(response.data)
                    context.commit("setValidation", response.data)
                }
            })
        },

        deleteBook(context, {id}) {
            context.commit('setDataPreloading', true)
            return window.axios.get(backendUrl + '/BooksApiController/delete/' + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 200) {
                    console.log("Book deleted successfully");
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getBooks');
                    router.push('/mainComponent');
                } else {
                    console.log(response.data)
                }
            })
        }
    }
})

export default store;